<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        return view('home', [
            'title' => 'haribahagia.pw'
        ]);
    }

    public function yuniRoby(Request $request)
    {
        $to = $request->to;

        return view('yuni-roby', [
            'title' => 'Yuni & Roby',
            'to' => $to
        ]);
    }

    public function ivanAnnisa(Request $request)
    {
        $to = $request->to;

        return view('ivan-annisa', [
            'title' => 'Ivan & Annisa',
            'to' => $to
        ]);
    }

    public function tes()
    {
        return view('tes', [
            'title' => 'tes'
        ]);
    }

    public function zakaPutri(Request $request)
    {
        $to = $request->to;

        return view('zaka-putri', [
            'title' => 'Zaka & Putri',
            'to' => $to
        ]);
    }
}
